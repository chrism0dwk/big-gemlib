"""Big sim with gemlib"""

from time import perf_counter
import numpy as np
import tensorflow as tf

from gemlib.distributions import DiscreteTimeStateTransitionModel


init_infec = tf.one_hot(0, depth=1200000, dtype=tf.float64)

initial_state = tf.stack(
    [
        1.0 - init_infec,
        init_infec,
        tf.zeros_like(init_infec),
        tf.zeros_like(init_infec),
        tf.zeros_like(init_infec),
        tf.zeros_like(init_infec),
    ],
    axis=-1,
)


def transition_rates(t, state):

    num_indivs = state.shape[0]
    se1 = 0.1 * state[:, 3] + 0.2 * state[:, 4]
    e1e2 = tf.fill((num_indivs,), tf.constant(0.5, tf.float64))
    e2i1 = tf.fill((num_indivs,), tf.constant(0.5, tf.float64))
    e2i2 = tf.fill((num_indivs,), tf.constant(0.5, tf.float64))
    i1r = tf.fill((num_indivs,), tf.constant(0.14, tf.float64))
    i2r = tf.fill((num_indivs,), tf.constant(0.14, tf.float64))

    return se1, e1e2, e2i1, e2i2, i1r, i2r


stoichiometry = np.array(
    [
        [-1, 1, 0, 0, 0, 0],
        [0, -1, 1, 0, 0, 0],
        [0, 0, -1, 1, 0, 0],
        [0, 0, -1, 0, 1, 0],
        [0, 0, 0, -1, 0, 1],
        [0, 0, 0, 0, -1, 1],
    ],
    dtype=np.float64,
)


def build_model(init_state):
    return DiscreteTimeStateTransitionModel(
        transition_rates, stoichiometry, init_state, 0, 1.0, 120
    )


simulator = tf.function(
    lambda init_state: build_model(init_state).sample(seed=[0, 0]), jit_compile=True
)

sim = simulator(initial_state)

start = perf_counter()
sim = simulator(initial_state)
end = perf_counter()

print(f"Time taken: {end-start} seconds")
